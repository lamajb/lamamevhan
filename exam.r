
# question 1
setwd("D:/data")


car.raw <- read.csv('carInsurance_train.csv')

car <- car.raw

str(car)
#install.packages('chron')

library(chron)
car$CallStart <- chron(times=car$CallStart)
car$CallEnd <- chron(times=car$CallEnd)
car$CallDuration <- car$CallEnd - car$CallStart

#install.packages('dplyr')
library(dplyr)
#car.fixed <- data.frame(car$CarInsurance,car$CallDuration,car$Job,car$Age,car$Marital,car$Education,car$Balance,car$NoOfContacts,car$Outcome,car$PrevAttempts, check.names=FALSE)
car.fixed <- car %>% select(CarInsurance, CallDuration, Job, Age, Marital, Education, Balance, NoOfContacts, Outcome, PrevAttempts)
# question 2

# make these factors
#car.fixed$CarInsurance <- as.factor(car.fixed$CarInsurance)
car.fixed$Job <- as.factor(car.fixed$Job)
car.fixed$Marital <- as.factor(car.fixed$Marital)
car.fixed$Education <- as.factor(car.fixed$Education)

# remove NA values
car.fixed$Outcome <- sapply(car$Outcome,changeNaVal,newval='unknown')

car.fixed$Outcome <- as.factor(car.fixed$Outcome)
# make time into int of seconds
car.fixed$CallDuration <- hours(car.fixed$CallDuration)*3600 + minutes(car.fixed$CallDuration)*60 + seconds(car.fixed$CallDuration)


coercx <- function(x,by){
  if(is.na(x)) return(by)
  if(x<=by) return (x)
  return(by)
}
coercxNeg <- function(x,by){
  if(is.na(x)) return(by)
  if(x>by) return (x)
  return(by)
}
changeNaVal <-function(x,newval){
  if(is.na(x)) return(newval)
  return(x)
}



# look at the tails and chose values from the graph based on how it looks
car.fixed$CallDuration <- sapply(car.fixed$CallDuration,coercx,by=1200)
car.fixed$PrevAttempts <- sapply(car.fixed$PrevAttempts,coercx,by=6)
car.fixed$Balance <- sapply(car.fixed$Balance,coercx,by=10000)
car.fixed$Balance <- sapply(car.fixed$Balance,coercxNeg,by=-700)
car.fixed$Age <- sapply(car.fixed$Age,coercx,by=77)


ggplot(car.fixed, aes(CallDuration, fill=CarInsurance))+geom_histogram()
ggplot(car.fixed, aes(Age, fill=CarInsurance))+geom_histogram()
ggplot(car.fixed, aes(Balance, fill=CarInsurance))+geom_histogram()
ggplot(car.fixed, aes(PrevAttempts, fill=CarInsurance))+geom_histogram()

ggplot(car.fixed, aes(Marital, fill=as.factor(CarInsurance)))+geom_bar(position = 'fill')


library(caTools)
# split as usual to 70/30
filter <- sample.split(car.fixed$CarInsurance, SplitRatio = 0.7)
car.fixed.train <- subset(car.fixed,filter == T)
car.fixed.test <- subset(car.fixed,filter == F)

#question 3
library(rpart)
library(rpart.plot)

model.dt <- rpart(CarInsurance ~ ., car.fixed.train)

#question 3 a
rpart.plot(model.dt,box.palette = "RdBu", shadow.col = "gray", nn = TRUE)
prediction <- predict(model.dt, car.fixed.test)
actual <- car.fixed.test$CarInsurance

cf <- table(actual,prediction>0.5)

precision <- cf['1','TRUE'] / (cf['1','TRUE']+cf['1','FALSE'])
recall <-cf['1','TRUE'] / (cf['1','TRUE']+cf['0','TRUE'])

#question 3 c
#install.packages("randomForest") 
library(randomForest)
model.rf <- randomForest(CarInsurance ~ ., car.fixed.train, importance = TRUE, na.action=na.exclude)

rpart.plot(model.rf,box.palette = "RdBu", shadow.col = "gray", nn = TRUE)
prediction.rf <- predict(model.rf, car.fixed.test)
actual.rf <- car.fixed.test$CarInsurance


cf.rf <- table(actual.rf,prediction.rf>0.6)

precision.rf <- cf.rf['1','TRUE'] / (cf.rf['1','TRUE']+cf.rf['1','FALSE'])
recall.rf <-cf.rf['1','TRUE'] / (cf.rf['1','TRUE']+cf.rf['0','TRUE'])

#question 3 d
library(pROC)

rocurve <- roc(actual, prediction, direction = ">", levels = c("1", "0"))
rocurve.rf <- roc(actual.rf, prediction.rf, direction = ">", levels = c("1", "0"))


plot(rocurve,col ='red', main = 'ROC Chart')
par(new = TRUE)
plot(rocurve.rf,col ='blue', main = 'ROC Chart')
#question 4 

varImpPlot(model.rf)
